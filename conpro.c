#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define BUFFSIZE 9

// two semaphore for put and get product
sem_t empty;
sem_t full;

// for critical section
pthread_mutex_t mutex;

int producer_id = 0;
int consumer_id = 0;

struct queue_type
{
	int buff[BUFFSIZE];
	int front;
	int rear;
}Q={{0}, 0, 0};

void* createItem()
{
	int id = producer_id;
	int i;
	producer_id++;
	while(1)
	{
		sem_wait(&empty); // wait until empty>0
		pthread_mutex_lock(&mutex);   // get lock to enter into critcal section
		Q.buff[Q.rear] = 1;  // put 1 to buffer
		printf("producer number <%d>, thread identifier:%lu put into buffer[%d]\n", id, pthread_self(), Q.rear);
		printf("the buffer now is: ");
		for(i = 0; i<BUFFSIZE; i++)
			printf("%d ", Q.buff[i]);
		printf("\n");
		Q.rear = (Q.rear+1)%BUFFSIZE;
		pthread_mutex_unlock(&mutex);
		sem_post(&full); // full++
	}
}


void* delItem()
{
	int id = consumer_id;
	int i;
	consumer_id++;
	while(1)
	{
		sem_wait(&full); //wait until full>0
		pthread_mutex_lock(&mutex);
		Q.buff[Q.front] = 0; // remove item from buffer
		printf("consumer number <%d>, thread identifier:%lu remove buffer[%d]\n", id, pthread_self(), Q.front);
		printf("the buffer now is: ");
		for(i = 0; i<BUFFSIZE; i++)
			printf("%d ", Q.buff[i]);
		printf("\n");
		Q.front = (Q.front+1)%BUFFSIZE;
		pthread_mutex_unlock(&mutex);
		sem_post(&empty); // empty++
	}
}

void main(int argc, char* argv[])
{
	int numPro = 6, numCon = 6; // num of producer and consumer respectively
	int ini_p, ini_c, ini_m;
	int i, ret;
	
	pthread_t producer[numPro];
	pthread_t consumer[numCon];
	
	// initialize full and empty semaphores
	ini_p = sem_init(&full, 0, 0);
	ini_c = sem_init(&empty, 0, BUFFSIZE);

	// if initialize semaphore success, then it will return 0
	if((ini_p || ini_c) != 0)
	{
		printf("sempahore initialize fail\n");
		exit(1);
	}
	
	ini_m = pthread_mutex_init(&mutex, NULL);
	if(ini_m != 0)
	{
		printf("mutex init fail\n");
		exit(1);
	}

	// create producer thread
        for(i = 0; i<numPro; i++)
	{
		ret = pthread_create(&producer[i], NULL, createItem, NULL);
		if(ret != 0)
		{
			printf("producer %d create fail\n", i);
			exit(1);
		}
	}
	
	// create consumer thread
	for(i = 0; i<numCon; i++)
	{
		ret = pthread_create(&consumer[i], NULL, delItem, NULL);
		if(ret != 0)
		{
			printf("consumer %d create fail\n", i);
			exit(1);
		}
	}

	// wait for producer thread
	// In this program, producer and consumer are in infinite loop, so we just only wait all producer thread to avoid main thread exists 
	for( i = 0; i<numPro; i++)
		pthread_join(producer[i], NULL);
	
	

}
